#include "all.h"

void
layout_win(void)
{
    int mrow,mcol;
    getmaxyx(stdscr, mrow, mcol);
    if (cmd_prompt)
        delwin(cmd_prompt->w);
    else
        cmd_prompt = prompt_new(0, COLOR_PAIR(0), ' ', 0, 0, 0, 0);
    cmd_prompt->w = newwin(1, mcol, 32, 0);
}

void
forceexit(int n)
{
    keyhandler_delete(ctx->kh);
    ctx->kh = 0;
    ctx->cur = 0;
    ctx->v = 0;
    vcursctx_delete(ctx);
    modes_free();
    delwin(cmd_prompt->w);
    prompt_delete(cmd_prompt);
    endwin();
    free(infofile);
    free(colorfile);
    free(yanked_info);
    calview_delete(cview);
    cal_delete(calendar);
    exit(n);    
}

void
tryexit(int n)
{
    if (unsavedchanges)
        weprintf("There are unsaved changes.");
    else
        forceexit(n);
}

void
print_all(void)
{
    if (cview)
        calview_scroll_print(cview);
    if (cmd_prompt)
        prompt_print(cmd_prompt);
}

#ifndef KEY_RESIZE
int resize = 0;
void
hresize(int i)
{
    resize = 1;
}
#endif
int ragetch(int err)
{
    int i;
    do
    {
        i = getch();
#ifdef KEY_RESIZE
        if (i==KEY_RESIZE) {
            layout_win();
            print_all();
        }
    } while(i == KEY_RESIZE || (!err && i == ERR));
#else
        if(resize) {
            endwin();
            layout_win();
            print_all();
            refresh();
            resize = 0;
        }
    } while(!err && i == ERR);
#endif
    return i;
}

prompt *cmd_prompt = 0;
char** errors = 0;
size_t s_errors = 0;

void
initcurses()
{
#ifndef KEY_RESIZE
    signal(SIGWINCH, hresize);
#endif
    initscr();
    cbreak();
    noecho();
    refresh();
    curs_set(0);
    keypad(stdscr, TRUE);
    if(has_colors() == FALSE) {
        endwin();
        printf("Your terminal does not support color\n");
        exit(1);
    }
    start_color();
    init_pair(1, COLOR_GREEN, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLACK);
    init_pair(3, COLOR_CYAN, COLOR_BLACK);
    init_pair(4, COLOR_YELLOW, COLOR_BLACK);
    init_pair(5, COLOR_BLUE, COLOR_BLACK);
    init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(7, COLOR_WHITE, COLOR_BLACK);
    init_pair(8, COLOR_BLACK, COLOR_WHITE);
    init_pair(9, COLOR_BLACK, COLOR_RED);
}


typedef struct {
    int reltohome;
    char *dir;
} configsearchpathitem;

configsearchpathitem configsearchpaths[] = {
    {1, "/lib/tscal/"},
    {1, "/.config/tscal/"},
    {0, ""}
};

int
getconfigfiles(char **infofile, char **colorfile) {
    char *homedir = getenv("HOME");
    char *infopath, *colorpath;
    for (size_t i = 0; i < LEN(configsearchpaths); i++) {
        char *dir = configsearchpaths[i].dir;
        infopath = configsearchpaths[i].reltohome ?
            strmcat3(homedir, dir, "info") : strmcat(dir, "info");
        colorpath = configsearchpaths[i].reltohome ?
            strmcat3(homedir, dir, "color") : strmcat(dir, "color");
        if (!access(infopath, R_OK) && !access(colorpath, R_OK)) {
            *infofile = infopath;
            *colorfile = colorpath;
            return 0;
        }
        free(infopath);
        free(colorpath);
    }
    return 1;
}

int
main() {
    setlocale(LC_ALL, "");
    if (getconfigfiles(&infofile, &colorfile)) {
        puts("Cannot open info and/or colorfile.");
        puts("If this is a new install choose one of the following for your calendar files:");
        puts("touch $HOME/lib/tscal/info; touch $HOME/lib/tscal/color");
        puts("touch $HOME/.config/tscal/info; touch $HOME/.config/tscal/color");
        puts("touch info; touch color");
        return 1;
    }

    initcurses();
    layout_win();

    int startofcalendar = 2015;
    int endofcalendar = MAX(today().year + 1, startofcalendar + 16);
    calendar = cal_new(startofcalendar, endofcalendar - startofcalendar + 1);
    if (cal_loadcaldata(calendar, infofile, colorfile))
        forceexit(1);
    unsavedchanges = 0;
    yanked_info = 0;
    yanked_color = 0;
    cview = calview_new(stdscr, calendar);
    calview_centerselected(cview);

    ctx = vcursctx_new(keyhandler_new(0), cview, 3);
    modes_init();
    ctx->modes[0] = mode_normal;
    ctx->modes[1] = mode_cmd;
    ctx->modes[2] = mode_insert;
    vcursctx_mode_switch(ctx, mode_normal);

    int ch = 0;
    while (1) {
        if ((ch = ragetch(1)) != ERR) {
            ctx->v = cview;
            vcursctx_handle_curses_key(ctx, ch);
        }
        if (errors) {
            cmd_prompt->visible = 1;
            cmd_prompt->attr = error_attr;
            cmd_prompt->prefix = 0;
            for (size_t i = 0; i < s_errors; i++) {
                prompt_setstr(cmd_prompt, errors[i]);
                prompt_print(cmd_prompt);
                if (ragetch(0) == 27)
                    break;
            }
            prompt_freebuf(cmd_prompt);
            for (size_t i = 0; i < s_errors; i++)
                free(errors[i]);
            s_errors = 0;
            free(errors);
            errors = 0;
            cmd_prompt->prefix = ' ';
            cmd_prompt->attr = default_attr;
            cmd_prompt->visible = 0;
            print_all();
        }
    }
    endwin(); /* End curses mode */
    return 0;
}
