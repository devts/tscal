#include "all.h"

calview*
calview_new(WINDOW* view, cal *c)
{
    calview *cv = emalloc(sizeof(calview));
    cv->c = c;
    cv->w = view;
    cv->width = 32;
    cv->selected = today();
    cv->startmonth = cv->selected;
    cv->input = prompt_new(0, COLOR_PAIR(0), ' ', 0, 0, 0, 0);
    cv->input->visible = 0;
    return cv;
}

void
calview_delete(calview *cv)
{
    if (cv->input)
        prompt_delete(cv->input);
    free(cv);
}

void
calview_centerselected(calview *cv)
{
    cv->startmonth = cv->selected;
    for (int i = 0; i < calview_getmws(cv)/2; i++)
        cv->startmonth = decmonth(cv->startmonth);
}

void
calview_incselectedday(calview *cv)
{
    cv->selected = incday(cv->selected); 
    calview_scroll(cv, 0);
}

void
calview_decselectedday(calview *cv)
{
    cv->selected = decday(cv->selected); 
    calview_scroll(cv, 0);
}

void
calview_incselectedmonth(calview *cv)
{
    cv->selected = incmonth(cv->selected);
    calview_scroll(cv, 0);
}

void
calview_decselectedmonth(calview *cv)
{
    cv->selected = decmonth(cv->selected);
    calview_scroll(cv, 0);
}

void
calview_setcolor(calview *cv, int color)
{
    caldata *cd = cal_allocandgetcaldata(cv->c, cv->selected);
    if (cd)
        cd->color = color;
}

int
calview_getmws(calview *cv)
{
    int mrow,mcol;
    getmaxyx(cv->w,mrow,mcol);
    return mcol/cv->width;
}

void
calview_printday(calview *cv, caldate date, int offsety, int offsetx)
{
    caldata* cd = cal_getcaldata(cv->c, date);
    int attr = calview_getattr(cv, cd, date);
    wattron(cv->w, attr);
    char *info = cd && cd->info ? cd->info : "";
    size_t str_msize = 12;
    char *str = emalloc(str_msize);
    snprintf(str, str_msize, "%s %02i.%02i. ", weekdayshorttostr(date), date.day, date.month);
    mvwaddnstr(cv->w, offsety, offsetx, str, str_msize);
    size_t npr = 0;
    size_t wcw = 0;
    wcwidth_seek_limited(info, -1, 0, &npr, &wcw, cv->width - 12);
    waddnstr(cv->w, info, utfbasetoend(info, -1, npr) + 1);
    free(str);
    wattroff(cv->w, attr);
}

void
calview_printmonth(calview *cv, caldate iter, int offsety, int offsetx)
{
    wattron(cv->w, COLOR_PAIR(2));
    mvwprintw(cv->w, offsety, offsetx, "%s %i", monthtostr(iter), iter.year);
    wattroff(cv->w, COLOR_PAIR(2));
    int daysofmonth = daysinmonth(iter);
    for (iter.day=1; iter.day<=daysofmonth; iter.day++) {
        calview_printday(cv, iter, offsety + iter.day, offsetx);
    }
    mvwvline(cv->w, offsety, offsetx + cv->width - 1, 0, 32);
}

void
calview_print(calview *cv)
{
    werase(cv->w);
    caldate iter = cv->startmonth;
    int mws = calview_getmws(cv);
    for(int i = 0; i < mws; i++) {
       calview_printmonth(cv, iter, 0, i*cv->width);
       iter = incmonth(iter);
    }
    wrefresh(cv->w);
    if (cv->input && cv->input->w)
        prompt_print(cv->input);
}

void
calview_scroll_print(calview *cv)
{
    calview_scroll(cv, 0);
    calview_print(cv);
}

void
calview_scroll(calview *cv, int i)
{
    /* if i is zero just bring the selection back in view */
    if (i == 0) {
        while((cv->selected.year - cv->startmonth.year)*12 + (cv->selected.month - cv->startmonth.month) < 0)
            cv->startmonth = decmonth(cv->startmonth);
        while((cv->selected.year - cv->startmonth.year)*12 + (cv->selected.month - cv->startmonth.month) - calview_getmws(cv) + 1 > 0)
            cv->startmonth = incmonth(cv->startmonth);
    } else if (i > 0) {
        while(i--)
            cv->startmonth = decmonth(cv->startmonth);
    } else {
        while(i++)
            cv->startmonth = incmonth(cv->startmonth);
    }
}

int
calview_getattr(calview *cv, caldata *cd, caldate date)
{
    int color = color_today && caldateequals(date, today()) ? color_today :
        (cd && cd->color ? cd->color : default_weekday_colors[weekday(date)]);
    int attr = COLOR_PAIR(color);
    if (caldateequals(date, cv->selected))
        attr |= A_REVERSE;
    return attr;
}
