void cmd_go(size_t argc, char** argv, char* cmd, void *v);
void cmd_write(size_t argc, char** argv, char* cmd, void *v);
void cmd_quit(size_t argc, char** argv, char* cmd, void *v);
void cmd_writequit(size_t argc, char** argv, char* cmd, void *v);
void cmd_forcequit(size_t argc, char** argv, char* cmd, void *v);
