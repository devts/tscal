#include "all.h"

/*Normal mode cmds*/
void
cmd_jk(unsigned int pre, key *post, const Arg *arg, void *v)
{
    calview *cv = v;
    pre = MAX(pre, 1);
    if (arg->i < 0) {
        for (int i = 0; i < pre; i++)
            calview_decselectedday(cv);
    } else {
        for (int i = 0; i < pre; i++)
            calview_incselectedday(cv);
    }
    calview_scroll_print(cv);
}

void
cmd_hl(unsigned int pre, key *post, const Arg *arg, void *v)
{
    calview *cv = v;
    pre = MAX(pre, 1);
    if (arg->i < 0) {
        for (int i = 0; i < pre; i++)
            calview_decselectedmonth(cv);
    } else {
        for (int i = 0; i < pre; i++)
            calview_incselectedmonth(cv);
    }
    calview_scroll_print(cv);
}

void
cmd_HL(unsigned int pre, key *post, const Arg *arg, void *v)
{
    calview *cv = v;
    pre = MAX(pre, 1);
    calview_scroll(cv, arg->i*pre);
    calview_scroll_print(cv);
}

void
cmd_resizecv(unsigned int pre, key *post, const Arg *arg, void *v)
{
    calview *cv = v;
    cv->width = MAX(cv->width + arg->i, 15);
    calview_scroll_print(cv);
}

void
cmd_setcolor(unsigned int pre, key *post, const Arg *arg, void *v)
{
    calview *cv = v;
    calview_setcolor(cv, arg->i);
    calview_scroll_print(cv);
    unsavedchanges = 1;
}

void
cmd_setinfo(unsigned int pre, key *post, const Arg *arg, void *v)
{
    vcursctx_mode_switch(ctx, mode_insert);
}

void
cmd_gg(unsigned int pre, key *post, const Arg *arg, void *v)
{
    calview *cv = v;
    cv->selected.day = 1;
    calview_scroll_print(cv);
}

void
cmd_gh(unsigned int pre, key *post, const Arg *arg, void *v)
{
    calview *cv = v;
    cv->selected = today();
    calview_centerselected(cv);
    calview_scroll_print(cv);
}

void
cmd_G(unsigned int pre, key *post, const Arg *arg, void *v)
{
    calview *cv = v;
    cv->selected.day = daysinmonth(cv->selected);
    calview_scroll_print(cv);
}

void
cmd_yank(unsigned int pre, key *post, const Arg *arg, void *v) {
    calview *cv = v;
    caldata *cd = cal_getcaldata(cv->c, cv->selected);
    free(yanked_info);
    yanked_info = cd->info ? estrdup(cd->info) : 0;
    yanked_color = cd->color;
    calview_scroll_print(cv);
}

void
cmd_cut(unsigned int pre, key *post, const Arg *arg, void *v) {
    calview *cv = v;
    caldata *cd = cal_getcaldata(cv->c, cv->selected);
    free(yanked_info);
    yanked_info = cd->info;
    cd->info = 0;
    yanked_color = cd->color;
    cd->color = 0;
    calview_scroll_print(cv);
    unsavedchanges = 1;
}

void
cmd_paste(unsigned int pre, key *post, const Arg *arg, void *v) {
    calview *cv = v;
    caldata *cd = cal_getcaldata(cv->c, cv->selected);
    free(cd->info);
    cd->info = yanked_info ? estrdup(yanked_info) : 0;
    cd->color = yanked_color;
    calview_scroll_print(cv);
    unsavedchanges = 1;
}

void
cmd_entercmd(unsigned int pre, key *post, const Arg *arg, void *v) {
    vcursctx_mode_switch(ctx, mode_cmd);
}

void
cmd_tryexit(unsigned int pre, key *post, const Arg *arg, void *v) {
    tryexit(0);
}

/* Insert / Cmd mode cmds*/
static void
mode_append_hist(mode *m, char *he)
{
    list_append((void **)&m->prompt_hist, &m->s_prompt_hist, &m->m_prompt_hist, sizeof(char *), &he);
    m->idx_hist_cur = m->s_prompt_hist;
}
static int
cmdmode_exec_prompt(void *v)
{
    if (!cmd_prompt->buf)
        return 0;
    int r = exec_cmd(cmd_prompt->buf, cmds, s_cmds, v);
    mode_append_hist(mode_cmd, strmcat("#", cmd_prompt->buf));
    prompt_freebuf(cmd_prompt);
    return r;
}
void
cmd_prompt_exec(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if (ctx->cur != mode_cmd)
        return;
    cmdmode_exec_prompt(v);
    vcursctx_mode_switch(ctx, mode_normal);
}
void
cmd_switch_hist(unsigned int pre, key *post, const Arg *arg, void *v)
{
    mode *m = ctx->cur;
    if (!m || !(m == mode_cmd || m == mode_insert))
        return;
    if (arg->i < 0)
        m->idx_hist_cur -= MIN(m->idx_hist_cur, -arg->i);
    else
        m->idx_hist_cur += MIN(m->s_prompt_hist - m->idx_hist_cur,  arg->i);
    char *he = 0;
    if (m->idx_hist_cur < m->s_prompt_hist)
        he = m->prompt_hist[m->idx_hist_cur];
    calview *cv = v;
    prompt *pr = m == mode_cmd ? cmd_prompt : cv->input;
    prompt_setstr(pr, he);
    prompt_print(pr);
}
void
cmd_prompt_exit(unsigned int pre, key *post, const Arg *arg, void *v)
{
    vcursctx_mode_switch(ctx, mode_normal);
}
void
cmd_prompt_curs_left(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    prompt_curs_left(p);
    prompt_print(p);
}
void
cmd_prompt_curs_right(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    prompt_curs_right(p);
    prompt_print(p);
}
void
cmd_prompt_curs_start(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    prompt_curs_start(p);
    prompt_print(p);
}
void
cmd_prompt_curs_end(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    prompt_curs_end(p);
    prompt_print(p);
}
void
cmd_prompt_curs_bs(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    prompt_curs_bs(p);
    prompt_print(p);
}
void
cmd_prompt_curs_dc(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    prompt_curs_dc(p);
    prompt_print(p);
}
void
cmd_prompt_curs_dl(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    prompt_freebuf(p);
    prompt_print(p);
}
void
cmd_prompt_ins(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    if (!post[0].utfk)
        return;
    prompt_ins(p, post[0].ku.r);
    prompt_print(p);
}
void
cmd_prompt_complete(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    prompt_complete(p);
    prompt_print(p);
}
void
cmd_prompt_ins_digraph(unsigned int pre, key *post, const Arg *arg, void *v)
{
    prompt *p = ctx->cur == mode_insert ? ((calview *)ctx->v)->input : cmd_prompt;
    if (!post[0].utfk || !post[1].utfk)
        return;
    if (post[0].ku.r < 0 || post[0].ku.r > 0x7F)
        return;
    if (post[1].ku.r < 0 || post[1].ku.r > 0x7F)
        return;
    Rune r = digraphtorune(post[0].ku.r, post[1].ku.r);
    if (r == Runeerror)
        return;
    prompt_ins(p, r);
    prompt_print(p);
}
static int
prompt_save(calview *cv) {
    caldata *cd = cal_allocandgetcaldata(cv->c, cv->selected);
    if (!cd)
        return -1;
    char *new_info = cv->input->buf && cv->input->buf_num ? estrdup(cv->input->buf) : 0;
    if (!cd->info && !new_info)
        return 0;
    if (cd->info && new_info && !strcmp(new_info, cd->info)) {
        free(new_info);
        return 0;
    }
    free(cd->info);
    cd->info = new_info;
    unsavedchanges = 1;
    return 1;
}
void
cmd_prompt_save(unsigned int pre, key *post, const Arg *arg, void *v)
{
    if (ctx->cur != mode_insert)
        return;
    calview *cv = v;
    if (prompt_save(cv) < 0)
        weprintf("error saving info");
    if (cv->input->buf)
        mode_append_hist(mode_insert, estrdup(cv->input->buf));
    vcursctx_mode_switch(ctx, mode_normal);
}
