#include "all.h"

int caldateequals(caldate a, caldate b)
{
    return a.day==b.day&&a.month==b.month&&a.year==b.year;
}

caldate incday(caldate a)
{
    ++a.day>daysinmonth(a)?(a.day=1, ++a.month>12?(a.month=1,++a.year):0):0;
    return a;
}
caldate decday(caldate a)
{
    --a.day<1?(--a.month<1?(a.month=12,--a.year):0),a.day=daysinmonth(a):0;
    return a;
}
caldate incmonth(caldate a)
{
    ++a.month>12?(a.month=1,++a.year):0;a.day=a.day>daysinmonth(a)?daysinmonth(a):a.day;
    return a;
}
caldate decmonth(caldate a)
{
    --a.month<1?(a.month=12,--a.year):0;a.day=a.day>daysinmonth(a)?daysinmonth(a):a.day;
    return a;
}

caldate today()
{
    time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime (&rawtime);
    caldate today;
    today.year = timeinfo->tm_year + 1900;
    today.month = timeinfo->tm_mon + 1;
    today.day = timeinfo->tm_mday;
    return today;
}

/* From http://www.tondering.dk/claus/cal/chrweek.php#calcdow */
int weekday(caldate date)
{
    int d = date.day;
    int m = date.month;
    int y = date.year;
    int a = (14 - m) / 12;
    y -= a;
    m += 12 * a - 2;
    return (d - 1 + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12) % 7;
}

static int
isleap(int y)
{
    if (y % 400 == 0)
        return 1;
    if (y % 100 == 0)
        return 0;
    return (y % 4 == 0);
}

int daysinmonth(caldate date)
{
    int m = date.month;
    int y = date.year;
    int mdays[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    return (m == 2 && isleap(y)) ? 29 : mdays[m];
}

char *months[]=
{
        " ",
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
};
char *weekdays[]={
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
};
char *weekdaysshort[]={
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
};

char* monthtostr(caldate date)
{
    int m = date.month;
    if(m>0&&m<13)
        return months[m];
    else
        return months[0];
}
char* weekdaytostr(caldate date)
{
    return weekdays[weekday(date)];
}
char* weekdayshorttostr(caldate date)
{
    return weekdaysshort[weekday(date)];
}

int
caldatevalid(caldate date)
{
    int m = date.month;
    int d = date.day;
    if (m < 1 || m > 12)
        return 0;
    if (d < 1 || d > daysinmonth(date))
        return 0;
    return 1;
}
