#include "../all.h"

int
exec_cmd(char* cmd, Cmd *cmds, size_t s_cmds, void *v)
{
    size_t pos = 0;
    while (cmd[++pos] && cmd[pos] != ' ') { }
    char* name;
    if (!(name = estrndup(cmd, pos)))
       return 1;
    size_t argc = 0;
    char** argv = 0;
    size_t spos;
    while (cmd[pos]) {
        while(cmd[pos] == ' ')
            pos++;
        if (cmd[pos] == '\'') {
            pos++;
            argv = ereallocarray(argv, ++argc, sizeof(char*));
            char *arg = 0;
            size_t argsize = 0;
            int cont;
            do {
                cont = 0;
                spos = pos;
                while (cmd[pos] && cmd[pos] != '\'')
                    pos++;
                if (cmd[pos] && cmd[pos + 1] == '\'') /* '' -> ' */ {
                    cont = 1;
                    pos++;
                }
                size_t targ = argsize;
                argsize += pos - spos;
                arg = ereallocarray(arg, argsize, 1);
                memcpy(arg + targ, cmd + spos, pos - spos);
                if (cmd[pos])
                    pos++;
            } while (cont);
            arg = ereallocarray(arg, ++argsize, 1);
            arg[argsize - 1] = 0;
            argv[argc - 1] = arg;
        } else {
            spos = pos;
            while (cmd[pos] && cmd[pos] != ' ')
                pos++;
            argv = ereallocarray(argv, ++argc, sizeof(char*));
            argv[argc - 1] = estrndup(&cmd[spos], pos - spos);
        }
    }
    int match = 0;
    for (size_t i = 0; i < s_cmds; i++) {
        if (!strcmp(name, cmds[i].name)) {
            match = 1;
            if (cmds[i].func)
                cmds[i].func(argc, argv, cmd, v);
            break;
        }
    }
    if (!match)
        weprintf("unknown command: %s", name ? name : "(null)");
    if (name)
        free(name);
    if (argv) {
        for (size_t i = 0; i < argc; i++)
            if (argv[i])
                free(argv[i]);
        free(argv);
    }
    return 0;
}
