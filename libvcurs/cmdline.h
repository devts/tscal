typedef struct {
    char* name;
    size_t s_name;
    void (*func)(size_t argc, char** argv, char* cmd, void *v);
} Cmd;

int exec_cmd(char* cmd, Cmd *cmds, size_t s_cmds, void *v);
