typedef struct {
    WINDOW *w;
    int attr;
    char prefix;
    int visible;

    void (*fstrchg)(char *, size_t, void *);
    void* argf;
    void (*complete)(char *, size_t, size_t, void *);
    void *argc;

    char *buf;
    size_t buf_num;
    size_t buf_msize;
    size_t curs;
    size_t scroll;
} prompt;

prompt *prompt_new(WINDOW *w, int attr, char prefix,
        void (*fstrchg)(char *, size_t, void *), void* argf,
        void (*complete)(char *, size_t, size_t, void *), void *argc);
void prompt_delete(prompt *p);
void prompt_setstr(prompt *p, char *s);
void prompt_freebuf(prompt *p);
void prompt_print(prompt *p);
void prompt_delete_line(prompt *p);
void prompt_curs_left(prompt *p);
void prompt_curs_right(prompt *p);
void prompt_curs_start(prompt *p);
void prompt_curs_end(prompt *p);
void prompt_curs_bs(prompt *p);
void prompt_curs_dc(prompt *p);
void prompt_ins(prompt *p, Rune r);
void prompt_complete(prompt *p);
