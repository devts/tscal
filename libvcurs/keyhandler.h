typedef union {
    int i;
    unsigned int ui;
    float f;
    const void *v;
} Arg;

typedef union {
    Rune r;
    int k; /*non unicode keys, e.g. Fn, Arrows*/
} keyu;

typedef struct {
    int utfk;
    keyu ku;
} key;

typedef struct {
    key* keys; /*seqence of keys that leads to the invocation of func*/
    size_t s_keys;
    /*func recieves the number typed before the Keybind in pre (if no nuber is typed 0 shall be passed)
     * and if s_post is greater than zero the next s_post characters typed afterwards
     * if s_post is zero func will be immediatley called without waiting for further input and post shall be NULL
     * in Addition it recieves Arg, so you can have the same function for mutliple Binds
     * the last argument shall be the tab the commad is expected to operate on*/
    size_t s_post;
    void (*func)(unsigned int pre, key *post, const Arg *, void *v);
    const Arg arg;
} keybind;

typedef struct {
    keybind *kbs;
    size_t s_kbs;
    keybind *fallthr;
    int do_pre;
} modebinds;

typedef struct {
    char uch[UTFmax];
    size_t num_uch;
    key *kb; /*key buffer*/
    size_t s_kb;
    size_t m_kb;
    key *post;
    size_t s_post;
    size_t m_post;
    unsigned int pre;
    int is_pre;
    int is_post;
    modebinds *m;
} keyhandler;

keyhandler *keyhandler_new(modebinds *m);
void keyhandler_delete(keyhandler *kh);
void keyhandler_reset(keyhandler *kh);
/* ck: key retured by getch to handle
 * v: caller defined argument for the keybinds in modebinds */
int keyhandler_handle_curses_key(keyhandler *kh, int ck, void *v);
void keyhandler_switch_mode(keyhandler *kh, modebinds *m);
