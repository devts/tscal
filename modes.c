#include "all.h"
mode *mode_normal = 0;
mode *mode_cmd = 0;
mode *mode_insert = 0;

void
setcmdline(char prefix,
        void (*fstrchg)(char *, size_t, void *), void* argf,
        void (*complete)(char *, size_t, size_t, void *), void *argc) 
{
    cmd_prompt->attr = default_attr;
    cmd_prompt->prefix = prefix;
    cmd_prompt->fstrchg = fstrchg;
    cmd_prompt->argf = argf;
    cmd_prompt->complete = complete;
    cmd_prompt->argc = argc;
    cmd_prompt->visible = 1;
    prompt_freebuf(cmd_prompt);
    curs_set(1);
    prompt_print(cmd_prompt);
}

void
unsetcmdline(void) {
    curs_set(0);
    cmd_prompt->prefix = ' ';
    cmd_prompt->fstrchg = 0;
    cmd_prompt->argf = 0;
    cmd_prompt->complete = 0;
    cmd_prompt->argc = 0;
    cmd_prompt->visible = 0;
    prompt_freebuf(cmd_prompt);
}

void
mode_normal_start(vcursctx *ctx, mode *m) {
    print_all();
}

void
mode_normal_end(vcursctx *ctx, mode *m) {
}

void
mode_cmd_start(vcursctx *ctx, mode *m) {
    setcmdline(':', 0, 0, 0, 0);
}

void
mode_cmd_end(vcursctx *ctx, mode *m) {
    unsetcmdline();
}

void
mode_insert_start(vcursctx *ctx, mode *m) {
    calview *cv = ctx->v;
    caldata *cd = cal_allocandgetcaldata(cv->c, cv->selected);
    size_t inputsize = MAX(cv->width - 11, 3);
    WINDOW *w = subwin(cv->w, 1, inputsize, cv->selected.day,
        ((cv->selected.year - cv->startmonth.year)*12 + (cv->selected.month - cv->startmonth.month))*cv->width + 10);
    cv->input->w = w;
    cv->input->attr = calview_getattr(cv, cd, cv->selected) & ~A_REVERSE;
    cv->input->prefix = ' ';
    cv->input->visible = 1;
    prompt_freebuf(cv->input);
    if (cd && cd->info)
        prompt_setstr(cv->input, cd->info);
    curs_set(1);
    prompt_print(cv->input);
}

void
mode_insert_end(vcursctx *ctx, mode *m) {
    calview *cv = ctx->v;
    cv->input->prefix = ' ';
    cv->input->attr = COLOR_PAIR(0);
    cv->input->visible = 0;
    prompt_freebuf(cv->input);
    curs_set(0);
    delwin(cv->input->w);
    cv->input->w = 0;
}

void
modes_init(void) {
    modebinds *nbinds = emalloc(sizeof(modebinds));
    *nbinds = (modebinds){normal_kbs, s_normal_kbs, normal_fallthr, 1};
    mode_normal = mode_new(0, 0, 0, 0, nbinds, mode_normal_start, mode_normal_end);

    modebinds *cbinds = emalloc(sizeof(modebinds));
    *cbinds = (modebinds){cmd_kbs, s_cmd_kbs, cmd_fallthr, 0};
    mode_cmd = mode_new(0, 0, 0, 0, cbinds, mode_cmd_start, mode_cmd_end);

    modebinds *sbinds = emalloc(sizeof(modebinds));
    *sbinds = (modebinds){insert_kbs, s_insert_kbs, insert_fallthr, 0};
    mode_insert = mode_new(0, 0, 0, 0, sbinds, mode_insert_start, mode_insert_end);
}

void modes_free(void) {
    if (mode_normal) {
        free(mode_normal->mb);
        mode_delete(mode_normal);
    }
    if (mode_cmd) {
        free(mode_cmd->mb);
        mode_delete(mode_cmd);
    }
    if (mode_insert) {
        free(mode_insert->mb);
        mode_delete(mode_insert);
    }
}
