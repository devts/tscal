typedef int32_t Rune;

enum {
	UTFmax    = 6,       /* maximum bytes per rune */
	Runeself  = 0x80,    /* rune and utf are equal (<) */
	Runeerror = 0xFFFD,  /* decoding error in utf */
	Runemax   = 0x10FFFF /* maximum rune value */
};

int badrune(Rune x);
int runelen(const Rune);
size_t runenlen(const Rune *, size_t);
unsigned int utfseq(char x);
int charntorune(Rune *, const char *, size_t);
int chartorune(Rune *, const char *);
int runetocharn(char *, size_t, const Rune *);
int runetochar(char *, const Rune *);
int runetocharm(char **, size_t *, Rune *);
int utftorunestrm(const char*, Rune **, size_t *);
int utfnext(const char *s, ssize_t len, size_t base_in, size_t *base_out);
int utfprev(const char *s, ssize_t len, size_t base_in, size_t *base_out);
int utfseek(const char *s, ssize_t len, size_t base_in, size_t *base_out, long off);
size_t wcwidth_range(char *c, ssize_t len, size_t base_start, size_t base_end);
size_t utfbasetoend(char *c, ssize_t len, size_t base_in);
int wcwidth_seek_limited(char *c, ssize_t len, size_t base_in, size_t *base_out, size_t *wcw_range, long max);
int fgetrune(Rune *, FILE *);
int efgetrune(Rune *, FILE *, const char *);
int fputrune(const Rune *, FILE *);
int efputrune(const Rune *, FILE *, const char *);
size_t utflen(const char *);
size_t utfnlen(const char *, size_t);
char *utfrune(const char *, Rune);
char *utfrrune(const char *, Rune);
char *utfutf(const char *, const char *);

int isalnumrune(Rune);
int isalpharune(Rune);
int isblankrune(Rune);
int iscntrlrune(Rune);
int isdigitrune(Rune);
int isgraphrune(Rune);
int islowerrune(Rune);
int isprintrune(Rune);
int ispunctrune(Rune);
int isspacerune(Rune);
int istitlerune(Rune);
int isupperrune(Rune);
int isxdigitrune(Rune);
Rune tolowerrune(Rune);
Rune toupperrune(Rune);

typedef struct {
    char a;
    char b;
    Rune r;
} Digraph;
Rune digraphtorune(char a, char b);
