#include "all.h"

/*key from char*/
#define KFC(x) {.utfk = 1, .ku = { .r = (Rune)(x) }}
/*key from int*/
#define KFI(x) {.utfk = 0, .ku = { .k = (int)(x) }}
#define CTRL(a) ((a) - 'a' + 1)

/*keyconfig*/
/* normal mode keybindings */
keybind normal_kbs_[] = {
    /* key                                  s_keys          s_post      function                     argument */
    { (key[]){ KFC('j')},                   1,              0,          cmd_jk,                      {.i = 1  }},
    { (key[]){ KFC('k')},                   1,              0,          cmd_jk,                      {.i = -1 }},
    { (key[]){ KFC('h')},                   1,              0,          cmd_hl,                      {.i = -1 }},
    { (key[]){ KFC('l')},                   1,              0,          cmd_hl,                      {.i = 1  }},
    { (key[]){ KFC('L')},                   1,              0,          cmd_HL,                      {.i = -1 }},
    { (key[]){ KFC('H')},                   1,              0,          cmd_HL,                      {.i = 1  }},
    { (key[]){ KFC('[')},                   1,              0,          cmd_resizecv,                {.i = -5 }},
    { (key[]){ KFC(']')},                   1,              0,          cmd_resizecv,                {.i = 5  }},
    { (key[]){ KFI(KEY_DOWN)},              1,              0,          cmd_jk,                      {.i = 1  }},
    { (key[]){ KFI(KEY_UP)},                1,              0,          cmd_jk,                      {.i = -1 }},
    { (key[]){ KFI(KEY_LEFT)},              1,              0,          cmd_hl,                      {.i = -1 }},
    { (key[]){ KFI(KEY_RIGHT)},             1,              0,          cmd_hl,                      {.i = 1  }},
    { (key[]){ KFC('\n')},                  1,              0,          cmd_setinfo,                 {.i = 0  }},
    { (key[]){ KFI(KEY_F(10))},             1,              0,          cmd_setcolor,                {.i = 0  }},
    { (key[]){ KFI(KEY_F(1))},              1,              0,          cmd_setcolor,                {.i = 1  }},
    { (key[]){ KFI(KEY_F(2))},              1,              0,          cmd_setcolor,                {.i = 2  }},
    { (key[]){ KFI(KEY_F(3))},              1,              0,          cmd_setcolor,                {.i = 3  }},
    { (key[]){ KFI(KEY_F(4))},              1,              0,          cmd_setcolor,                {.i = 4  }},
    { (key[]){ KFI(KEY_F(5))},              1,              0,          cmd_setcolor,                {.i = 5  }},
    { (key[]){ KFI(KEY_F(6))},              1,              0,          cmd_setcolor,                {.i = 6  }},
    { (key[]){ KFI(KEY_F(7))},              1,              0,          cmd_setcolor,                {.i = 7  }},
    { (key[]){ KFI(KEY_F(8))},              1,              0,          cmd_setcolor,                {.i = 8  }},
    { (key[]){ KFI(KEY_F(9))},              1,              0,          cmd_setcolor,                {.i = 9  }},
    { (key[]){ KFC('q')},                   1,              0,          cmd_tryexit,                 {.i = 0  }},
    { (key[]){ KFC('G')},                   1,              0,          cmd_G,                       {.i = 0  }},
    { (key[]){ KFC(':')},                   1,              0,          cmd_entercmd,                {.i = 0  }},
    { (key[]){ KFC('g'), KFC('g')},         2,              0,          cmd_gg,                      {.i = 0  }},
    { (key[]){ KFC('g'), KFC('h')},         2,              0,          cmd_gh,                      {.i = 0  }},
    { (key[]){ KFC('y'), KFC('y')},         2,              0,          cmd_yank,                    {.i = 0  }},
    { (key[]){ KFC('d'), KFC('d')},         2,              0,          cmd_cut,                     {.i = 0  }},
    { (key[]){ KFC('p'), KFC('p')},         2,              0,          cmd_paste,                   {.i = 0  }},
};
keybind *normal_kbs = normal_kbs_;
size_t s_normal_kbs = LEN(normal_kbs_);
keybind *normal_fallthr = 0;

keybind kb_prompt_ins =
    { (key[]){ KFC('j')},                   1,              0,          cmd_prompt_ins,                      {.i =1  },                          };

/* cmd mode keybindings */
keybind cmd_kbs_[] = {
	/* key                                  s_keys          s_post      function                     argument                            */
    { (key[]){ KFC(27)},                    1,              0,          cmd_prompt_exit,             {.i =1  },                          },
    { (key[]){ KFI(KEY_LEFT)},              1,              0,          cmd_prompt_curs_left,        {.i =1  },                          },
    { (key[]){ KFI(KEY_RIGHT)},             1,              0,          cmd_prompt_curs_right,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('a'))},             1,              0,          cmd_prompt_curs_start,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('e'))},             1,              0,          cmd_prompt_curs_end,         {.i =1  },                          },
    { (key[]){ KFI(KEY_BACKSPACE)},         1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFC(127)},                   1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFI(KEY_DC)},                1,              0,          cmd_prompt_curs_dc,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('u'))},             1,              0,          cmd_prompt_curs_dl,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('k'))},             1,              2,          cmd_prompt_ins_digraph,      {.i =1  },                          },
    { (key[]){ KFC('\t')},                  1,              0,          cmd_prompt_complete,         {.i =1  },                          },
    { (key[]){ KFC('\n')},                  1,              0,          cmd_prompt_exec,             {.i =1  },                          },
    { (key[]){ KFI(KEY_UP)},                1,              0,          cmd_switch_hist,             {.i =-1 },                          },
    { (key[]){ KFI(KEY_DOWN)},              1,              0,          cmd_switch_hist,             {.i =1  },                          },
};
keybind *cmd_kbs = cmd_kbs_;
size_t s_cmd_kbs = LEN(cmd_kbs_);
keybind *cmd_fallthr = &kb_prompt_ins;

/* insert mode keybindings */
keybind insert_kbs_[] = {
	/* key                                  s_keys          s_post      function                     argument                            */
    { (key[]){ KFC(27)},                    1,              0,          cmd_prompt_exit,             {.i =1  },                          },
    { (key[]){ KFI(KEY_LEFT)},              1,              0,          cmd_prompt_curs_left,        {.i =1  },                          },
    { (key[]){ KFI(KEY_RIGHT)},             1,              0,          cmd_prompt_curs_right,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('a'))},             1,              0,          cmd_prompt_curs_start,       {.i =1  },                          },
    { (key[]){ KFC(CTRL('e'))},             1,              0,          cmd_prompt_curs_end,         {.i =1  },                          },
    { (key[]){ KFI(KEY_BACKSPACE)},         1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFC(127)},                   1,              0,          cmd_prompt_curs_bs,          {.i =1  },                          },
    { (key[]){ KFI(KEY_DC)},                1,              0,          cmd_prompt_curs_dc,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('u'))},             1,              0,          cmd_prompt_curs_dl,          {.i =1  },                          },
    { (key[]){ KFC(CTRL('k'))},             1,              2,          cmd_prompt_ins_digraph,      {.i =1  },                          },
    { (key[]){ KFC('\t')},                  1,              0,          cmd_prompt_complete,         {.i =1  },                          },
    { (key[]){ KFC('\n')},                  1,              0,          cmd_prompt_save,             {.i =1  },                          },
    { (key[]){ KFI(KEY_UP)},                1,              0,          cmd_switch_hist,             {.i =-1 },                          },
    { (key[]){ KFI(KEY_DOWN)},              1,              0,          cmd_switch_hist,             {.i =1  },                          },
};
keybind *insert_kbs = insert_kbs_;
size_t s_insert_kbs = LEN(insert_kbs_);
keybind *insert_fallthr = &kb_prompt_ins;

/*cmdline prefs*/
Cmd _cmds[] = {
    /* name         s_name      func    */
    { "w",          1,          cmd_write       },
    { "q",          1,          cmd_quit        },
    { "go",         2,          cmd_go          },
    { "wq",         2,          cmd_writequit   },
    { "q!",         2,          cmd_forcequit   },
};
Cmd *cmds = _cmds;
size_t s_cmds = LEN(_cmds);

int _default_weekday_colors[] = {
    0, /* Monday */
    0, /* Tuesday */
    0, /* Wednesday */
    0, /* Thursday */
    0, /* Friday */
    1, /* Saturday */
    1, /* Sunday  */
};
int *default_weekday_colors = _default_weekday_colors;
int color_today = 2;
int error_attr = COLOR_PAIR(2) | A_BOLD;
int default_attr = COLOR_PAIR(0);
