#include "all.h"

void
cmd_go(size_t argc, char** argv, char* cmd, void *v) {
    calview *cv = v;
    if (argc > 1) {
        weprintf("Invalid argument count.");
        return;
    }
    caldate cd;
    if (!argc) {
        cd = today();
    } else {
        if (sscanf(argv[0], "%04d-%02d-%02d", &cd.year, &cd.month, &cd.day) != 3 || !caldatevalid(cd)) {
            weprintf("Invalid date.");
            return;
        }
    }
    cv->selected = cd;
    calview_centerselected(cv);
}

void
cmd_write(size_t argc, char** argv, char* cmd, void *v) {
    if (argc) {
        weprintf("Invalid argument count.");
        return;
    }
    if (cal_savecaldata(calendar, infofile, colorfile)) {
        weprintf("Error: Saving data failed!");
        return;
    }
    unsavedchanges = 0;
}

void
cmd_quit(size_t argc, char** argv, char* cmd, void *v) {
    if (argc) {
        weprintf("Invalid argument count.");
        return;
    }
    tryexit(0);
}

void
cmd_writequit(size_t argc, char** argv, char* cmd, void *v) {
    if (argc) {
        weprintf("Invalid argument count.");
        return;
    }
    if (cal_savecaldata(calendar, infofile, colorfile)) {
        weprintf("Error: Saving data failed!");
        return;
    }
    unsavedchanges = 0;
    tryexit(0);
}

void
cmd_forcequit(size_t argc, char** argv, char* cmd, void *v) {
    if (argc) {
        weprintf("Invalid argument count.");
        return;
    }
    forceexit(0);
}
