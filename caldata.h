typedef struct caldata {
    int color; /* 0 = default color for the particular weekday */
    char* info; /* holds actual info about appointments etc. */
} caldata;

typedef struct {
    int startyear;
    int yod; /* years of data */
    caldata **data;
} cal;

cal *cal_new(int startyear, int yod);
void cal_delete(cal *c);

/* extend the year range for which caldata can be stored, but do not allocate caldata space */
void cal_extendrange(cal *c, int startyear, int yod);
/* allocate caldata storage for the specified year */
/* return value: 0=success, otherwise fail */
int cal_allocyear(cal *c, int year);

/* retrieve caldata for date if available */
caldata* cal_getcaldata(cal *c, caldate date);
/* retrieve caldata for date, existing if available, newly allocated otherwise */
caldata* cal_allocandgetcaldata(cal *c, caldate date);

/* return value: 0=success, otherwise fail */
int cal_loadcaldata(cal *c, char* infofile, char* colorfile);
/* return value: 0=success, otherwise fail */
int cal_savecaldata(cal *c, char* infofile, char* colorfile);
