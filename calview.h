typedef struct calview {
    WINDOW* w;
    int width;
    cal *c;
    caldate selected;
    caldate startmonth;
    prompt *input;
} calview;

calview *calview_new(WINDOW* view, cal *c);
void calview_delete(calview *cv);

void calview_centerselected(calview *cv);
void calview_incselectedday(calview *cv);
void calview_decselectedday(calview *cv);
void calview_incselectedmonth(calview *cv);
void calview_decselectedmonth(calview *cv);
int calview_getmws(calview *cv);
int calview_getattr(calview *cv, caldata *cd, caldate date);
void calview_print(calview *cv);
void calview_scroll_print(calview *cv);
void calview_scroll(calview *cv, int i);
void calview_setcolor(calview *cv, int color);
