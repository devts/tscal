typedef struct caldate {
   int year;
   int month;
   int day;
} caldate;

caldate incday(caldate a);
caldate decday(caldate a);
caldate incmonth(caldate a);
caldate decmonth(caldate a);
int caldateequals(caldate, caldate);
caldate today();
int weekday(caldate);
int daysinmonth(caldate);
char* monthtostr(caldate);
char* weekdaytostr(caldate date);
char* weekdayshorttostr(caldate date);
int caldatevalid(caldate date);
