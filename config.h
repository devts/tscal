/*keyconfig*/
keybind *normal_kbs;
size_t s_normal_kbs;
keybind *normal_fallthr;

keybind *cmd_kbs;
size_t s_cmd_kbs;
keybind *cmd_fallthr;

keybind *insert_kbs;
size_t s_insert_kbs;
keybind *insert_fallthr;

/*cmdline prefs*/
Cmd *cmds;
size_t s_cmds;

/*colorscheme*/
int *default_weekday_colors;
int color_today;
int error_attr;
int default_attr;
