#include "../all.h"

void *
emalloc(size_t size)
{
    void *p;
    while (!(p = malloc(size)))
        sleep(1u);
    return p;
}

void *
ecalloc(size_t nmemb, size_t size)
{
    void *p;
    while (!(p = calloc(nmemb, size)))
        sleep(1u);
    return p;
}

void *
erealloc(void *ptr, size_t size)
{
    void *p;
    while (!(p = realloc(ptr, size)))
        sleep(1u);
    return p;
}

/*
 * This is sqrt(SIZE_MAX+1), as s1*s2 <= SIZE_MAX
 * if both s1 < MUL_NO_OVERFLOW and s2 < MUL_NO_OVERFLOW
 */
#define MUL_NO_OVERFLOW	(1UL << (sizeof(size_t) * 4))

void *
reallocarray(void *optr, size_t nmemb, size_t size)
{
	if ((nmemb >= MUL_NO_OVERFLOW || size >= MUL_NO_OVERFLOW) &&
	    nmemb > 0 && SIZE_MAX / nmemb < size) {
		errno = ENOMEM;
		return NULL;
	}
	return realloc(optr, size * nmemb);
}

void *
ereallocarray(void *optr, size_t nmemb, size_t size)
{
	void *p;
	while (!(p = reallocarray(optr, nmemb, size)))
        sleep(1u);
	return p;
}

