#include "../all.h"
char** errors;
size_t s_errors;

void
enprintf(int status, const char *fmt, ...)
{
    endwin();

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	exit(status);
}

void
weprintf(const char *fmt, ...)
{
	va_list ap;
    errors = ereallocarray(errors, ++s_errors, sizeof(char*));
    errors[s_errors - 1] = emalloc(PATH_MAX);
    char buf[PATH_MAX];
    strlcpy(buf, fmt, sizeof(buf));
	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
        strlcat(buf, " ", sizeof(buf));
        char *serr = strerror(errno);
        if(serr)
            strlcat(buf, serr, sizeof(buf));
    }

	va_start(ap, fmt);
	vsnprintf(errors[s_errors - 1], PATH_MAX, buf, ap);
	va_end(ap);
}
