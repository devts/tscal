#include "../all.h"

void
list_minsize(void **l, size_t *len, size_t *msize, size_t itemsize, size_t min) {
  if(min <= *msize)
      return;
  *msize = MAX(16, MAX(min, *msize << 1));
  *l = ereallocarray(*l, *msize, itemsize);
}

void
list_append(void **l, size_t *len, size_t *msize, size_t itemsize, void *item) {
  list_insert_at(l, len, msize, itemsize, *len, item);
}

void
list_append_range(void **l, size_t *len, size_t *msize, size_t itemsize, size_t numitems, void *items) {
  list_insert_range_at(l, len, msize, itemsize, *len, numitems, items);
}

void *
lfind_r(const void *key, const void *base, size_t *nelp,
	size_t width, int (*compar)(const void *, const void *, void *), void *v) {
    char (*p)[width] = (void *)base;
    size_t n = *nelp;
    size_t i;

    for (i = 0; i < n; i++)
        if (compar(key, p[i], v) == 0)
            return p[i];
    return 0;
}

int
list_findcmpfn(const void *a, const void *b, void *v) {
    size_t itemsize = *(size_t *)v;
    return memcmp(a, b, itemsize);
}

void
list_find(void **l, size_t *len, size_t *msize, size_t itemsize, void *item, char **v, size_t *i) {
  assert(v);
  *v = lfind_r(item, *l, len, itemsize, list_findcmpfn, &itemsize);
  if (!v)
    return;
  *i = (*v - (char *)(*l))/itemsize;
}

void
list_remove(void **l, size_t *len, size_t *msize, size_t itemsize, void *item) {
  size_t i;
  char *v;
  list_find(l, len, msize, itemsize, item, &v, &i);
  if (!v)
    return;
  list_remove_idx(l, len, msize, itemsize, i);
}

void
list_remove_idx(void **l, size_t *len, size_t *msize, size_t itemsize, size_t idx) {
  list_remove_idx_range(l, len, msize, itemsize, idx, 1);
}

void
list_remove_idx_range(void **l, size_t *len, size_t *msize, size_t itemsize, size_t idx, size_t num) {
  if (!num)
    return;
  char *v = (char *)*l + idx*itemsize;
  if (idx + num < *len)
    memmove(v, v + itemsize*num, itemsize * (*len - num - idx));
  (*len)-=num;
}

void
list_insert_at(void **l, size_t *len, size_t *msize, size_t itemsize, size_t idx, void *item) {
  list_insert_range_at(l, len, msize, itemsize, idx, 1, item);
}

void
list_insert_range_at(void **l, size_t *len, size_t *msize, size_t itemsize, size_t idx, size_t numitems, void *items) {
  if (!numitems)
    return;
  assert(idx <= *len);
  (*len)+= numitems;
  list_minsize(l, len, msize, itemsize, *len);
  if (idx != *len - 1)
      memmove((char*)*l+itemsize*(idx+numitems), (char*)*l+itemsize*(idx), itemsize*(*len - numitems - idx));
  memcpy((char*)*l+itemsize*(idx), items, itemsize*numitems);
}

void
list_malloc_trim(void **l, size_t *len, size_t *msize, size_t itemsize) {
  *msize = *len;
  *l = ereallocarray(*l, *msize, itemsize);
}
