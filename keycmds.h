/* Normal mode cmds */
void cmd_jk(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_hl(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_HL(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_resizecv(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_setcolor(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_gg(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_gh(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_G(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_yank(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_cut(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_paste(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_entercmd(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_tryexit(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_setinfo(unsigned int pre, key *post, const Arg *arg, void *v);

/* Insert / Cmd mode cmds */
void cmd_prompt_exec(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_switch_hist(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_exit(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_curs_left(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_curs_right(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_curs_start(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_curs_end(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_curs_bs(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_curs_dc(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_curs_dl(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_ins(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_complete(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_ins_digraph(unsigned int pre, key *post, const Arg *arg, void *v);
void cmd_prompt_save(unsigned int pre, key *post, const Arg *arg, void *v);
