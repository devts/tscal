#include "all.h"

static caldata*
cal_getcaldataymd(cal *c, int y, int m, int d)
{
    return c->data[y] + m*31 + d;
}

cal*
cal_new(int startyear, int yod)
{
    assert(yod > 0);
    cal *c = emalloc(sizeof(cal));
    c->startyear = startyear;
    c->yod = yod;
    c->data = ecalloc(yod, sizeof(caldata*));
    return c;
}

void
cal_delete(cal *c)
{
    for (int y = 0; y < c->yod ; y++) {
        if (!c->data[y])
            continue;
        for (int m = 0; m < 12; m++)
            for (int d = 0; d < 31; d++)
                free(cal_getcaldataymd(c, y, m, d)->info);
        free(c->data[y]);
    }
    free(c->data);
    free(c);
}

void
cal_extendrange(cal *c, int startyear, int yod)
{
    if (yod < 1)
        return;
    if (startyear >= c->startyear && startyear + yod <= c->startyear + c->yod)
        return;
    int new_startyear = MIN(startyear, c->startyear);
    int new_endyear = MAX(startyear + yod - 1, c->startyear + c->yod - 1);
    int new_yod = new_endyear - new_startyear + 1;
    caldata **new_data = ecalloc(new_yod, sizeof(caldata*));
    int idxstart = c->startyear - new_startyear;
    memcpy(new_data + idxstart, c->data, c->yod*sizeof(caldata*));
    free(c->data);
    c->data = new_data;
    c->startyear = new_startyear;
    c->yod = new_yod;
}

int
cal_allocyear(cal *c, int year)
{
    if (year < c->startyear || year >= c->startyear + c->yod)
        return 1;
    int y = year - c->startyear;
    if (c->data[y])
        return 0;
    c->data[y] = emalloc(12*31*sizeof(caldata));
    for (int m = 0; m < 12; m++) {
        for (int d = 0; d < 31; d++) {
            caldata *cald = cal_getcaldataymd(c, y, m, d);
            cald->color = 0;
            cald->info = 0; 
        }
    }
    return 0;
}

caldata*
cal_getcaldata(cal *c, caldate date)
{
    if (date.year < c->startyear || date.year >= c->startyear + c->yod)
        return 0;
    if (!c->data[date.year - c->startyear])
        return 0;
    return cal_getcaldataymd(c, date.year - c->startyear, date.month - 1, date.day - 1);
}

caldata*
cal_allocandgetcaldata(cal *c, caldate date)
{
    if (date.year < c->startyear || date.year >= c->startyear + c->yod)
        cal_extendrange(c, date.year, 1);
    if (!c->data[date.year - c->startyear])
        cal_allocyear(c, date.year);
    return cal_getcaldata(c, date);
}

int
cal_loadcaldata(cal *c, char* infofile, char* colorfile)
{
    FILE *inf,*cf;

    inf = fopen(infofile, "r");
    if (!inf)
        return 1;
    cf = fopen(colorfile, "r");
    if (!cf)
        return fclose(inf), 1;
    int i = 0;
    caldate cd;
    size_t n = 0;
    char *buff = 0;
    errno = 0;
    while (getline(&buff, &n, cf) >= 0) {
        if (sscanf(buff, "%04d-%02d-%02d %i\n", &cd.year, &cd.month, &cd.day, &i) != 4 || !caldatevalid(cd))
            return fclose(cf), fclose(inf), 1;
        cal_allocandgetcaldata(c, cd)->color = i;
        free(buff);
        buff = 0;
        n = 0;
    }
    if (errno)
        return fclose(cf), fclose(inf), 1;
    while (getline(&buff, &n, inf) >= 0) {
        char* info = 0;
        if (sscanf(buff, "%04d-%02d-%02d %m[^\n]\n", &cd.year, &cd.month, &cd.day, &info) != 4 || !caldatevalid(cd))
            return free(info), fclose(cf), fclose(inf), 1;
        caldata *d = cal_allocandgetcaldata(c, cd);
        free(d->info);
        d->info = info;
        free(buff);
        buff = 0;
        n = 0;
    }
    if (errno)
        return fclose(cf), fclose(inf), 1;
    fclose(cf);
    fclose(inf);
    free(buff);
    return 0;
}

int
cal_savecaldata(cal *c, char* infofile, char* colorfile)
{
    FILE *inf, *cf;
    char *infofile_new = strmcat(infofile, "_new");
    char *colorfile_new = strmcat(colorfile, "_new");
    inf = fopen(infofile_new, "w+");
    if (!inf)
        return free(infofile_new), free(colorfile_new), 1;
    cf = fopen(colorfile_new, "w+");
    if (!cf)
        return free(infofile_new), free(colorfile_new), fclose(inf), 1;

    for (int y = 0; y < c->yod; y++) {
        if (!c->data[y])
            continue;
        for (int m = 0; m < 12; m++) {
            for (int d = 0; d < 31; d++) {
                caldata *cd = cal_getcaldataymd(c, y, m, d);
                if (cd->color)
                    if (fprintf(cf, "%04d-%02d-%02d %i\n", c->startyear+y, m+1, d+1, cd->color) < 0)
                        return free(infofile_new), free(colorfile_new), fclose(cf), fclose(inf), 1;
                if (cd->info && strcmp(cd->info, ""))
                    if (fprintf(inf, "%04d-%02d-%02d %s\n", c->startyear+y, m+1, d+1, cd->info) < 0)
                        return free(infofile_new), free(colorfile_new), fclose(cf), fclose(inf), 1;
            }
        }
    }
    if (fclose(inf) | fclose(cf))
        return free(infofile_new), free(colorfile_new), 1;
    if (unlink(infofile) | unlink(colorfile))
        return free(infofile_new), free(colorfile_new), 1;
    if (rename(infofile_new, infofile))
        return free(infofile_new), free(colorfile_new), 1;
    if (rename(colorfile_new, colorfile))
        return free(infofile_new), free(colorfile_new), 1;
    return free(infofile_new), free(colorfile_new), 0;
}
